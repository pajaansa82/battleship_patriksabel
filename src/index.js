// importit
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import ship from "./img/ship.jpg";
import bomb from "./img/bomb.jpg";

// vakiot
const gameConstants = {
    name: "Battleship",
    amountSquares: 25,
    amountShips: 3,
    maxGuesses: 15,
    amountHits:0
};

//palauttaa renderSquare() -funktiolle button elementin
function Square(props) {
    return (
        <button className="square" onClick={props.onClick}>
          {props.value}
        </button>
    );
}    

// board luokka
class Board extends React.Component {      
    constructor(props){
        super(props);
        
        this.state ={
            shipsLeft: gameConstants.amountShips,
            guesses: 0,
            seconds: 0,
            squares: Array(gameConstants.amountSquares).fill(null),
            ships: this.randomShips()
        };
    }    
    
    // käsittele klikkauksen laudalla
    handleClick(i) {
        let guesses = this.state.guesses;
        const squares = this.state.squares.slice();
          
        guesses++;
        this.setState({guesses: guesses});

            if (this.state.guesses < gameConstants.maxGuesses) {
                if (this.state.ships.indexOf(i) === -1) {
                 squares[i] = <img src={bomb} alt="X" />;

                }
                else {
                    squares[i] = <img src={ship} alt="O" />;
                    gameConstants.amountShips--;
                    gameConstants.amountHits++;
                }
        this.setState({
        squares: squares
        });
        }
    }

    //palauttaa render() -funktiolle elementin
    renderSquare(i) {
        return (
            <Square
                value={this.state.squares[i]}
                onClick={() => this.handleClick(i)}/>            
        );    
    }
            
    //Arpoo laivat     
    randomShips() {      
        let ships = [];
            for (let i = 0; i < 3; i++){
                let randomNumber = Math.floor(Math.random() * 25 + 1);   
                
                if (ships.length > 0){
                    
                    if (ships.indexOf(randomNumber) === -1) {
                        ships[i] = randomNumber;    
                    
                    } else if (ships.indexOf(randomNumber) !== -1) {
                        i--;
                    }
                    
                } else if(ships.length === 0){
                    ships[i] = randomNumber;
                }
            }
        return ships;    
    }
         
    // Laskuri laskee peliaikaa
    tick() {
        this.setState(state => ({
        seconds: state.seconds + 1
        }));   
    }
    
    componentDidMount() {        
        this.interval = setInterval(() => this.tick(), 1000);
    }
    
    componentWillUnmount() {
        clearInterval(this.interval);
    }

    //renderöi pelilaudan
    render() {
      return (
        <div>
          <h1>{gameConstants.name}</h1>
          <div>Hits: {gameConstants.amountHits}</div>

          <div>Bombs remaining: {gameConstants.maxGuesses - this.state.guesses}</div>
          <div>Ships remaining: {gameConstants.amountShips}</div>
          <p></p>
          <div className="board-row">
            {this.renderSquare(0)}
            {this.renderSquare(1)}
            {this.renderSquare(2)}
            {this.renderSquare(3)}
            {this.renderSquare(4)}
          </div>
          <div className="board-row">
            {this.renderSquare(5)}
            {this.renderSquare(6)}
            {this.renderSquare(7)}
            {this.renderSquare(8)}
            {this.renderSquare(9)}
          </div>
          <div className="board-row">
            {this.renderSquare(10)}
            {this.renderSquare(11)}
            {this.renderSquare(12)}
            {this.renderSquare(13)}
            {this.renderSquare(14)}
          </div>
          <div className="board-row">
            {this.renderSquare(15)}
            {this.renderSquare(16)}
            {this.renderSquare(17)}
            {this.renderSquare(18)}
            {this.renderSquare(19)}
          </div>
          <div className="board-row">
            {this.renderSquare(20)}
            {this.renderSquare(21)}
            {this.renderSquare(22)}
            {this.renderSquare(23)}
            {this.renderSquare(24)}
          </div>
          <p></p>
          <form onSubmit={this.handleSubmit}>
                  <button>Start game</button>
                  <p>Seconds: {this.state.seconds}</p>
          </form>
        </div>
      );
    }
};

//game luokka/pelilaudalle div
class Game extends React.Component {
    render() {
      return (
          <div className="game-board">
            <Board />
          </div>
      );
    }
};

//renderöi pelin
ReactDOM.render(
    <Game />,
    document.getElementById("root")
);
